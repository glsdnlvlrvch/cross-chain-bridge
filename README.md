# :space_invader: Cross-chain bridge
Basic implementation of cross-chain bridge smart contract with validator for swapping ERC20 tokens between ETH and BSC.

**ETHEREUM RINKEBY:** </br> https://rinkeby.etherscan.io/address/0x3d0654c2460595A5d1a1E142A120aA93c264c7Fb#code </br> </br>
**BINANCE SMART CHAIN TESTNET:** </br> https://testnet.bscscan.com/address/0x3d0654c2460595A5d1a1E142A120aA93c264c7Fb#code

## Tasks
- accounts (standart hardhat task to get accounts)
- swap (swap erc20 tokens in blockchain #1)
- redeem (redeem erc20 tokens in blockchain #2)
- addtoken (add erc20 token for swapping)
- deletetoken (delete erc20 token for swapping)

