//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import "./IBridge.sol";
import "./IToken.sol";


contract Bridge is IBridge, Ownable {
    using ECDSA for bytes32;

    address private _validator;

    mapping(string => address) public tokens;
    mapping(uint256 => bool) public chainIds;
    mapping(bytes => bool) private _signatures;

    event swapInitialized(
        address indexed recipient, 
        uint256 indexed amount, 
        uint256 indexed chainId,
        address sender,
        uint256 nonce,
        string symbol
    );

    constructor(address validator) {
        _validator = validator;
    }

    function swap(
        address recipient, 
        uint256 amount, 
        uint256 chainId, 
        uint256 nonce, 
        string memory symbol
    ) public override {
        require(
            chainIds[chainId] == true,
            "invalid chain id"
        );

        address token = tokens[symbol];
        require(
            token != address(0),
            "invalid symbol"
        );

        IToken(token).burn(msg.sender, amount);

        emit swapInitialized(
            recipient,
            amount,
            chainId,
            msg.sender,
            nonce,
            symbol
        );
    }

    function redeem(
        address sender,
        address recipient, 
        uint256 amount, 
        uint256 chainFrom,
        uint256 nonce,
        string memory symbol,
        bytes memory signature
    ) public override {
        require(
            chainIds[chainFrom] == true,
            "invalid chain id"
        );

        address token = tokens[symbol];
        require(
            token != address(0),
            "invalid symbol"
        );

        require(
            !_signatures[signature],
            "Replay attack prevented"
        );
        
        bytes32 _hash = keccak256(
            abi.encodePacked(
                sender,
                recipient,
                amount,
                chainFrom,
                nonce,
                symbol
            )
        );

        require(
            _verify(_hash, signature, _validator),
            "invalid signature"
        );

        _signatures[signature] = true;
        IToken(token).mint(recipient, amount);
    }

    function _verify(bytes32 data, bytes memory signature, address account) internal pure returns (bool) {
        return data
            .toEthSignedMessageHash()
            .recover(signature) == account;
    }

    // ADMIN functions 
    function addToken(
        string memory symbol,
        address token
    ) public onlyOwner {
        tokens[symbol] = token;
    }

    function deleteToken(
        string memory symbol
    ) public onlyOwner {
        tokens[symbol] = address(0);
    }

    function addChain(
        uint256 chainId
    ) public onlyOwner {
        chainIds[chainId] = true;
    }

    function deleteChain(
        uint256 chainId
    ) public onlyOwner {
        chainIds[chainId] = false;
    }
}