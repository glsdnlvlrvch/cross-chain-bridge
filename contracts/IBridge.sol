//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

interface IBridge {
    function swap(
        address recipient, 
        uint256 amount, 
        uint256 chainId, 
        uint256 nonce, 
        string memory symbol
    ) external;

   function redeem(
        address sender,
        address recipient, 
        uint256 amount, 
        uint256 chainFrom,
        uint256 nonce,
        string memory symbol,
        bytes memory signature
    ) external;
}