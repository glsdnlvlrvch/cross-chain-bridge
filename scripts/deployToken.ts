import { ethers } from "hardhat";
import * as dotenv from "dotenv";
dotenv.config();

async function main() {
    const TokenFactory = await ethers.getContractFactory("Token");
    const token = await TokenFactory.deploy(
        "Kakazhstan token",
        "KZT"
        process.env.BRIDGE
    );

    await token.deployed();

    console.log("Token deployed to:", token.address);
}

main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});
