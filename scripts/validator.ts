import * as dotenv from "dotenv";
import { ethers } from "ethers";
import bridgeJson from "../artifacts/contracts/Bridge.sol/Bridge.json";

dotenv.config();

const privateKey = process.env.PRIVATE_KEY || '';
const bridgeEthAddr = process.env.BRIDGE_ETH || '';
const bridgeBscAddr = process.env.BRIDGE_BSC || '';
const bscUrl = process.env.BSC_TESTNET_URL || '';
const ethUrl = process.env.RINKEBY_URL || '';

const wallet = new ethers.Wallet(privateKey)

const providerEth = new ethers.providers.JsonRpcProvider(ethUrl);
const providerBsc = new ethers.providers.JsonRpcProvider(bscUrl);

const bridgeEth = new ethers.Contract(
    bridgeEthAddr,
    bridgeJson.abi,
    providerEth
)

const bridgeBsc = new ethers.Contract(
    bridgeBscAddr,
    bridgeJson.abi,
    providerBsc
)

bridgeEth.on("swapInitialized", async (sender, recipient, amount, chainId, nonce, symbol) => {
    const signer = wallet.connect(providerBsc);

    const msgHash = ethers.utils.solidityKeccak256(
        [
            "address",
            "address",
            "uint256",
            "uint256",
            "uint256",
            "string"
        ],
        [
            sender,
            recipient,
            amount,          // amount
            chainId,        // chainfrom
            nonce,          // nonce
            symbol
        ]
    )
    const bytesArray = ethers.utils.arrayify(msgHash);
    const signature = await signer.signMessage(bytesArray);

    console.log('Signature: ', signature);
});

bridgeBsc.on("swapInitialized", async (recipient, amount, chainId, sender, nonce, symbol) => {
    const signer = wallet.connect(providerEth);
    
    const msgHash = ethers.utils.solidityKeccak256(
        [
            "address",
            "address",
            "uint256",
            "uint256",
            "uint256",
            "string"
        ],
        [
            sender,
            recipient,
            amount,          // amount
            chainId,        // chainfrom
            nonce,          // nonce
            symbol
        ]
    )
    const bytesArray = ethers.utils.arrayify(msgHash);
    const signature = await signer.signMessage(bytesArray);

    console.log('Signature: ', signature);
});