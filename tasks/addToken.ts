import { task } from "hardhat/config";


task("addtoken", "Add new erc20 token in swapping pool")
    .addParam("token", "address of token")
    .addParam("symbol", "symbol of token")
    .addParam("bridge", "bridge to swap tokens")
    .setAction(async (taskArgs, hre) => {
        const contract = await hre.ethers.getContractAt("Bridge", taskArgs.bridge);
        await contract.addToken(
            taskArgs.symbol,
            taskArgs.token,
        );
    });