import { task } from "hardhat/config";


task("redeem", "Redeem tokens")
    .addParam("sender", "address of sender")
    .addParam("recipient", "address of recipient")
    .addParam("amount", "amount of tokens")
    .addParam("chain", "chainId of blockchain")
    .addParam("nonce", "unique nonce")
    .addParam("symbol", "symbol of token")
    .addParam("signature", "signature by validator")
    .addParam("bridge", "bridge to swap tokens")
    .setAction(async (taskArgs, hre) => {
        const contract = await hre.ethers.getContractAt("Bridge", taskArgs.bridge);
        await contract.redeem(
            taskArgs.sender,
            taskArgs.recipient,
            taskArgs.amount,
            taskArgs.chain,
            taskArgs.nonce,
            taskArgs.symbol,
            taskArgs.signature
        );
    });