import { task } from "hardhat/config";


task("swap", "Swap tokens")
    .addParam("recipient", "address of recipient")
    .addParam("amount", "amount of tokens")
    .addParam("chain", "chainId of blockchain")
    .addParam("nonce", "unique nonce")
    .addParam("symbol", "symbol of token")
    .addParam("bridge", "bridge to swap tokens")
    .setAction(async (taskArgs, hre) => {
        const contract = await hre.ethers.getContractAt("Bridge", taskArgs.bridge);
        await contract.swap(
            taskArgs.recipient,
            taskArgs.amount,
            taskArgs.chain,
            taskArgs.nonce,
            taskArgs.symbol
        );
    });