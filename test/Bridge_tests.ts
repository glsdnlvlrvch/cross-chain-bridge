import { expect } from "chai";
import { ethers } from "hardhat";
import { Contract, Signer, BigNumber } from "ethers";

describe("Cross-chain bridge", function () {
    const name: string = "Kazakhstan token";
    const symbol: string = "KZT";
    // TODO: .env without hardcode

    let validator: string;
    let token: Contract;
    let bridge: Contract;

    let signers: Signer[];

    beforeEach(async function () {
        signers = await ethers.getSigners();
        validator = await signers[0].getAddress();
        
        let Factory1 = await ethers.getContractFactory("Bridge");
        bridge = await Factory1.deploy(
            validator
        );
        await bridge.deployed();

        let Factory2 = await ethers.getContractFactory("Token");
        token = await Factory2.deploy(
            name,
            symbol,
            bridge.address
        );
        await token.deployed();

        await bridge.addToken(
            symbol,
            token.address
        );

        await bridge.addChain(111); // test chain

        const owner = await signers[0].getAddress();
        const minter_role = ethers.utils.keccak256(
            ethers.utils.toUtf8Bytes("MINTER_ROLE")
        );

        await token.grantRole(minter_role, owner);
    });

    it("swap && redeem", async () => {
        const sender = await signers[0].getAddress();
        const recipient = await signers[1].getAddress();

        await token.connect(signers[0]).mint(sender, 10);

        expect(
            await token.balanceOf(sender)
        ).to.be.eq(10);

        await expect(
            bridge.swap(
                await signers[1].getAddress(),
                7,
                112,
                0,
                symbol
            )
        ).to.be.revertedWith("invalid chain id");

        await expect(
            bridge.swap(
                await signers[1].getAddress(),
                7,
                111,
                0,
                "fakeSymbol"
            )
        ).to.be.revertedWith("invalid symbol");

        const tx = await bridge.swap(
            await signers[1].getAddress(),
            7,
            111,
            0,
            symbol
        )

        expect(
            await token.balanceOf(sender)
        ).to.be.eq(3);

        expect(tx).to.emit(bridge, "swapInitialized").withArgs(
            await signers[1].getAddress(),
            7,
            111,
            await signers[0].getAddress(),
            0,
            symbol
        )
        
        // back-end gives us a signature
        const signer: Signer = signers[0];

        let msgHash = ethers.utils.solidityKeccak256(
            [
                "address",
                "address",
                "uint256",
                "uint256",
                "uint256",
                "string"
            ],
            [
                sender,
                recipient,
                7,          // amount
                111,        // chainfrom
                0,          // nonce
                symbol
            ]
        )
        const bytesArray = ethers.utils.arrayify(msgHash);
        const signature = await signer.signMessage(bytesArray);

        await bridge.redeem(
            sender,
            recipient,
            7,          // amount
            111,        // chainfrom
            0,          // nonce
            symbol,
            signature
        )

        await expect(
            bridge.redeem(
                sender,
                recipient,
                7,          // amount
                111,        // chainfrom
                0,          // nonce
                symbol,
                signature
            )
        ).to.be.revertedWith("Replay attack prevented");

        expect(
            await token.balanceOf(recipient)
        ).to.be.eq(7);

        const fakeSignature = await signers[4].signMessage(bytesArray);
        expect(
            bridge.redeem(
                sender,
                recipient,
                7,          // amount
                111,        // chainfrom
                0,          // nonce
                symbol,
                fakeSignature
            )
        ).to.be.revertedWith("invalid signature")


    });

    describe("Admin functions", function () {
        it("addToken && deleteToken", async () => {
            let Factory2 = await ethers.getContractFactory("Token");
            let ftoken = await Factory2.deploy(
                "fakeName",
                "fakeSymbol",
                bridge.address
            );
            await ftoken.deployed();
    
            await bridge.addToken(
                "fakeSymbol",
                ftoken.address
            )

            let addr = await bridge.tokens("fakeSymbol");
            expect(addr).to.be.eq(ftoken.address);

            await bridge.deleteToken(
                "fakeSymbol"
            );
            
            addr = await bridge.tokens("fakeSymbol");
            expect(addr).to.be.eq("0x0000000000000000000000000000000000000000");
        });

        it("addChain && deleteChain", async () => {
            await bridge.addChain(
                777
            );
            
            let flag = await bridge.chainIds(777);
            expect(flag).to.be.true;

            await bridge.deleteChain(
                777
            );

            flag = await bridge.chainIds(777);
            expect(flag).to.be.false;
        });
    })
});
